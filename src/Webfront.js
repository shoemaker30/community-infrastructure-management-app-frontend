import React from "react";
import FormSubmit from "./FormSubmit";

export default class Webfront extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      branches: [],
      options: [],
      current_branch: null,
      branch_stack: null,
      buttonsSelected: []
    };
    this.back = this.back.bind(this);
    this.load = this.load.bind(this);
  }

  componentDidMount() {
    let branches = [];
    let tt = this;

    fetch(this.props.branches)
      .then(res => res.json())
      .then(
        result => {
          //branches = result;
          result.forEach(function(res) {
            branches[res.id] = res;
            if (res.root) {
              tt.state.current_branch = res.id;
              tt.state.branch_stack = [res.id];
            }
          });
          console.log(branches);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );

    fetch(this.props.options)
      .then(res => res.json())
      .then(
        result => {
          let i = 0;
          result.forEach(option => {
            // TODO: remove once options have database IDs
            option.id = i++;
            if (branches[option.branchId].options === undefined)
              branches[option.branchId].options = [];
            branches[option.branchId].options[option.id] = option;
          });

          this.setState({
            isLoaded: true,
            branches: branches
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  load(leaf) {
    const { branch_stack } = this.state;
    branch_stack.push(leaf);
    this.setState({ current_branch: leaf, branch_stack: branch_stack });
  }

  select(id) {
    let { buttonsSelected } = this.state;
    if (buttonsSelected.indexOf(id) > -1)
      buttonsSelected.splice(buttonsSelected.indexOf(id), 1);
    else buttonsSelected.push(id);
    this.setState({ buttonsSelected: buttonsSelected });
  }

  selectProg(id) {
    let { buttonsSelected } = this.state;
    buttonsSelected = [id];
    this.setState({ buttonsSelected: buttonsSelected });
  }

  generateStyle(button) {
    const { buttonsSelected } = this.state;
    let buttonStyle = { width: "300px" };

    if (buttonsSelected.indexOf(button) > -1)
      buttonStyle.backgroundColor = "#b2b2ff";

    return buttonStyle;
  }

  // TODO: split submit form into a separate component

  back() {
    // TODO: clear selection
    const { branch_stack } = this.state;
    if (branch_stack.length === 0) {
      this.setState({
        current_branch: "5d535f42928bd44840666397",
        branch_stack: []
      });
    } else {
      branch_stack.pop();
      this.setState({
        current_branch: branch_stack[branch_stack.length - 1],
        branch_stack: branch_stack
      });
    }
  }

  formRend(id) {
    const { branches, current_branch, buttonsSelected } = this.state;

    switch (id) {
      case 0:
        return (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            {branches[current_branch].root !== true ? (
              <p>
                <button onClick={this.back}>Back</button>
              </p>
            ) : null}
            <p>{branches[current_branch].text}</p>
            <ul style={{ listStyleType: "none" }}>
              {branches[current_branch].options.map(opt => (
                <li key={opt.id}>
                  <button
                    onClick={() => this.load(opt.leaf)}
                    style={this.generateStyle(opt)}
                  >
                    {opt.text}
                  </button>
                </li>
              ))}
            </ul>
          </div>
        );
      case 1:
        return (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            <p>
              <button onClick={this.back}>Back</button>
            </p>
            <p>{branches[current_branch].text}</p>
            <ul
              style={{
                listStyleType: "none",
                display: "flex",
                width: "750px",
                justifyContent: "space-between"
              }}
            >
              {branches[current_branch].options.map(opt => (
                <li
                  key={opt.id}
                  style={{
                    display: "flex",
                    width: "150px",
                    alignItems: "center",
                    flexDirection: "column"
                  }}
                >
                  <button
                    onClick={() => {
                      this.selectProg(opt);
                    }}
                    style={
                      branches[current_branch].options.indexOf(
                        buttonsSelected[buttonsSelected.length - 1]
                      ) >= branches[current_branch].options.indexOf(opt)
                        ? { width: 30, backgroundColor: "#b2b2ff" }
                        : { width: 30 }
                    }
                  >
                    *
                  </button>
                  <br />
                  {opt.text}
                </li>
              ))}
            </ul>
            <FormSubmit selectedOptions={buttonsSelected} />
          </div>
        );
      case 2:
        return (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            <p>
              <button onClick={this.back}>Back</button>
            </p>
            <p>{branches[current_branch].text}</p>
            <ul style={{ listStyleType: "none" }}>
              {branches[current_branch].options.map(opt => (
                <li key={opt.id}>
                  <button
                    onClick={() => this.select(opt)}
                    style={this.generateStyle(opt)}
                  >
                    {opt.text}
                  </button>
                </li>
              ))}
            </ul>
            <FormSubmit selectedOptions={buttonsSelected} />
          </div>
        );
      default:
        return (
          <div>
            <p>
              This element could not be rendered! Please report this issue to
              the developer :)
            </p>
            <p>
              <button onClick={this.back}>Back</button>
            </p>
          </div>
        );
    }
  }

  render() {
    const {
      error,
      isLoaded,
      branches,
      current_branch,
      buttonsSelected
    } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      // TODO: change display based on branches[current_branch].style
      // TODO: split this up into different option components and pass the click item upstream

      let cb = current_branch;
      if (current_branch === 0) {
        branches.forEach(function(b) {
          if (b.root === true) {
            cb = b.id;
          }
        });
      }

      if (typeof branches[cb] !== "undefined") {
        return this.formRend(branches[cb].style);
      } else {
        return (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column"
            }}
          >
            <p>
              <button onClick={this.back}>Back</button>
            </p>
            <FormSubmit selectedOptions={buttonsSelected} />
          </div>
        );
      }
    }
  }
}
